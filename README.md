# Queen's rock

Lightweight QR-code extracting and decoding library.

## Features

- no unsafe
- no dependencies
- WASM supported

## Usage

See [example](examples/scanner.rs).
