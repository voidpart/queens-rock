extern crate image;
extern crate queens_rock;

use image::GenericImageView;
use queens_rock::Scanner;

use std::env;

#[derive(Debug)]
struct Code {
    pub error: Option<String>,
    pub text: Option<String>,
}

fn main() {
    let file = env::args().nth(1).unwrap();

    let image = image::open(file).unwrap();
    let luma = image.to_luma().into_vec();

    let scanner = Scanner::new(
        luma.as_ref(),
        image.width() as usize,
        image.height() as usize,
    );
    let scan_results = scanner.scan();

    let mut results: Vec<Code> = Vec::new();
    for i in 0..scan_results.len() {
        let code = scan_results
            .extract(i)
            .unwrap()
            .decode()
            .and_then(|i| i.try_string());

        match code {
            Ok(data) => {
                results.push(Code {
                    error: None,
                    text: Some(data),
                });
            }
            Err(_) => {
                results.push(Code {
                    error: Some(String::from("Something went wrong")),
                    text: None,
                });
            }
        };
    }

    println!("{:?}", results);
}
