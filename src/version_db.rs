pub const MAX_VERSION: usize = 40;

#[derive(Debug, Default, Copy, Clone)]
pub struct RsParams {
    pub bs: u8, /* Small block size */
    pub dw: u8, /* Small data words */
    pub ns: u8, /* Number of small blocks */
}

#[derive(Debug, Default, Copy, Clone)]
pub struct VersionInfo {
    pub data_bytes: usize,
    pub apat: &'static [u8],
    pub idx: u8,
    pub ecc: [RsParams; 4],
}

impl VersionInfo {
    pub fn size(&self) -> usize {
        self.idx as usize * 4 + 17
    }

    pub fn reserved_cell(&self, i: usize, j: usize) -> bool {
        let info = self;

        let (i, j) = (i as isize, j as isize);
        let size = self.size();
        let mut ai = -1;
        let mut aj = -1;

        /* Finder + format: top left */
        if i < 9 && j < 9 {
            return true;
        }

        /* Finder + format: bottom left */
        if i + 8 >= size as isize && j < 9 {
            return true;
        }

        /* Finder + format: top right */
        if i < 9 && j + 8 >= size as isize {
            return true;
        }

        /* Exclude timing patterns */
        if i == 6 || j == 6 {
            return true;
        }

        /* Exclude version info, if it exists. Version info sits adjacent to
         * the top-right and bottom-left finders in three rows, bounded by
         * the timing pattern.
         */
        if self.idx >= 7 {
            if i < 6 && j + 11 >= size as isize {
                return true;
            }

            if i + 11 >= size as isize && j < 6 {
                return true;
            }
        }

        /* Exclude alignment patterns */
        let mut a = 0;
        for p in info.apat.iter().cloned().take_while(|i| *i != 0) {
            let p = p as isize;

            if (p - i).abs() < 3 {
                ai = a;
            }

            if (p - j).abs() < 3 {
                aj = a;
            }

            a += 1;
        }

        if ai >= 0 && aj >= 0 {
            a -= 1;

            if ai > 0 && ai < a {
                return true;
            }

            if aj > 0 && aj < a {
                return true;
            }

            if aj == a && ai == a {
                return true;
            }
        }

        false
    }
}

pub const VERSION_DB: [VersionInfo; 41] = [
    VersionInfo {
        idx: 0,
        data_bytes: 0,
        apat: &[],
        ecc: [RsParams {
            bs: 0,
            dw: 0,
            ns: 0,
        }; 4],
    },
    VersionInfo {
        idx: 1,
        data_bytes: 26,
        apat: &[0],
        ecc: [
            RsParams {
                bs: 26,
                dw: 16,
                ns: 1,
            },
            RsParams {
                bs: 26,
                dw: 19,
                ns: 1,
            },
            RsParams {
                bs: 26,
                dw: 9,
                ns: 1,
            },
            RsParams {
                bs: 26,
                dw: 13,
                ns: 1,
            },
        ],
    },
    VersionInfo {
        idx: 2,
        data_bytes: 44,
        apat: &[6, 18],
        ecc: [
            RsParams {
                bs: 44,
                dw: 28,
                ns: 1,
            },
            RsParams {
                bs: 44,
                dw: 34,
                ns: 1,
            },
            RsParams {
                bs: 44,
                dw: 16,
                ns: 1,
            },
            RsParams {
                bs: 44,
                dw: 22,
                ns: 1,
            },
        ],
    },
    VersionInfo {
        idx: 3,
        data_bytes: 70,
        apat: &[6, 22],
        ecc: [
            RsParams {
                bs: 70,
                dw: 44,
                ns: 1,
            },
            RsParams {
                bs: 70,
                dw: 55,
                ns: 1,
            },
            RsParams {
                bs: 35,
                dw: 13,
                ns: 2,
            },
            RsParams {
                bs: 35,
                dw: 17,
                ns: 2,
            },
        ],
    },
    VersionInfo {
        idx: 4,
        data_bytes: 100,
        apat: &[6, 26],
        ecc: [
            RsParams {
                bs: 50,
                dw: 32,
                ns: 2,
            },
            RsParams {
                bs: 100,
                dw: 80,
                ns: 1,
            },
            RsParams {
                bs: 25,
                dw: 9,
                ns: 4,
            },
            RsParams {
                bs: 50,
                dw: 24,
                ns: 2,
            },
        ],
    },
    VersionInfo {
        idx: 5,
        data_bytes: 134,
        apat: &[6, 30],
        ecc: [
            RsParams {
                bs: 67,
                dw: 43,
                ns: 2,
            },
            RsParams {
                bs: 134,
                dw: 108,
                ns: 1,
            },
            RsParams {
                bs: 33,
                dw: 11,
                ns: 2,
            },
            RsParams {
                bs: 33,
                dw: 15,
                ns: 2,
            },
        ],
    },
    VersionInfo {
        idx: 6,
        data_bytes: 172,
        apat: &[6, 34],
        ecc: [
            RsParams {
                bs: 43,
                dw: 27,
                ns: 4,
            },
            RsParams {
                bs: 86,
                dw: 68,
                ns: 2,
            },
            RsParams {
                bs: 43,
                dw: 15,
                ns: 4,
            },
            RsParams {
                bs: 43,
                dw: 19,
                ns: 4,
            },
        ],
    },
    VersionInfo {
        idx: 7,
        data_bytes: 196,
        apat: &[6, 22, 38],
        ecc: [
            RsParams {
                bs: 49,
                dw: 31,
                ns: 4,
            },
            RsParams {
                bs: 98,
                dw: 78,
                ns: 2,
            },
            RsParams {
                bs: 39,
                dw: 13,
                ns: 4,
            },
            RsParams {
                bs: 32,
                dw: 14,
                ns: 2,
            },
        ],
    },
    VersionInfo {
        idx: 8,
        data_bytes: 242,
        apat: &[6, 24, 42],
        ecc: [
            RsParams {
                bs: 60,
                dw: 38,
                ns: 2,
            },
            RsParams {
                bs: 121,
                dw: 97,
                ns: 2,
            },
            RsParams {
                bs: 40,
                dw: 14,
                ns: 4,
            },
            RsParams {
                bs: 40,
                dw: 18,
                ns: 4,
            },
        ],
    },
    VersionInfo {
        idx: 9,
        data_bytes: 292,
        apat: &[6, 26, 46],
        ecc: [
            RsParams {
                bs: 58,
                dw: 36,
                ns: 3,
            },
            RsParams {
                bs: 146,
                dw: 116,
                ns: 2,
            },
            RsParams {
                bs: 36,
                dw: 12,
                ns: 4,
            },
            RsParams {
                bs: 36,
                dw: 16,
                ns: 4,
            },
        ],
    },
    VersionInfo {
        idx: 10,
        data_bytes: 346,
        apat: &[6, 28, 50],
        ecc: [
            RsParams {
                bs: 69,
                dw: 43,
                ns: 4,
            },
            RsParams {
                bs: 86,
                dw: 68,
                ns: 2,
            },
            RsParams {
                bs: 43,
                dw: 15,
                ns: 6,
            },
            RsParams {
                bs: 43,
                dw: 19,
                ns: 6,
            },
        ],
    },
    VersionInfo {
        idx: 11,
        data_bytes: 404,
        apat: &[6, 30, 54],
        ecc: [
            RsParams {
                bs: 80,
                dw: 50,
                ns: 1,
            },
            RsParams {
                bs: 101,
                dw: 81,
                ns: 4,
            },
            RsParams {
                bs: 36,
                dw: 12,
                ns: 3,
            },
            RsParams {
                bs: 50,
                dw: 22,
                ns: 4,
            },
        ],
    },
    VersionInfo {
        idx: 12,
        data_bytes: 466,
        apat: &[6, 32, 58],
        ecc: [
            RsParams {
                bs: 58,
                dw: 36,
                ns: 6,
            },
            RsParams {
                bs: 116,
                dw: 92,
                ns: 2,
            },
            RsParams {
                bs: 42,
                dw: 14,
                ns: 7,
            },
            RsParams {
                bs: 46,
                dw: 20,
                ns: 4,
            },
        ],
    },
    VersionInfo {
        idx: 13,
        data_bytes: 532,
        apat: &[6, 34, 62],
        ecc: [
            RsParams {
                bs: 59,
                dw: 37,
                ns: 8,
            },
            RsParams {
                bs: 133,
                dw: 107,
                ns: 4,
            },
            RsParams {
                bs: 33,
                dw: 11,
                ns: 12,
            },
            RsParams {
                bs: 44,
                dw: 20,
                ns: 8,
            },
        ],
    },
    VersionInfo {
        idx: 14,
        data_bytes: 581,
        apat: &[6, 26, 46, 66],
        ecc: [
            RsParams {
                bs: 64,
                dw: 40,
                ns: 4,
            },
            RsParams {
                bs: 145,
                dw: 115,
                ns: 3,
            },
            RsParams {
                bs: 36,
                dw: 12,
                ns: 11,
            },
            RsParams {
                bs: 36,
                dw: 16,
                ns: 11,
            },
        ],
    },
    VersionInfo {
        idx: 15,
        data_bytes: 655,
        apat: &[6, 26, 48, 70],
        ecc: [
            RsParams {
                bs: 65,
                dw: 41,
                ns: 5,
            },
            RsParams {
                bs: 109,
                dw: 87,
                ns: 5,
            },
            RsParams {
                bs: 36,
                dw: 12,
                ns: 11,
            },
            RsParams {
                bs: 54,
                dw: 24,
                ns: 5,
            },
        ],
    },
    VersionInfo {
        idx: 16,
        data_bytes: 733,
        apat: &[6, 26, 50, 74],
        ecc: [
            RsParams {
                bs: 73,
                dw: 45,
                ns: 7,
            },
            RsParams {
                bs: 122,
                dw: 98,
                ns: 5,
            },
            RsParams {
                bs: 45,
                dw: 15,
                ns: 3,
            },
            RsParams {
                bs: 43,
                dw: 19,
                ns: 15,
            },
        ],
    },
    VersionInfo {
        idx: 17,
        data_bytes: 815,
        apat: &[6, 30, 54, 78],
        ecc: [
            RsParams {
                bs: 74,
                dw: 46,
                ns: 10,
            },
            RsParams {
                bs: 135,
                dw: 107,
                ns: 1,
            },
            RsParams {
                bs: 42,
                dw: 14,
                ns: 2,
            },
            RsParams {
                bs: 50,
                dw: 22,
                ns: 1,
            },
        ],
    },
    VersionInfo {
        idx: 18,
        data_bytes: 901,
        apat: &[6, 30, 56, 82],
        ecc: [
            RsParams {
                bs: 69,
                dw: 43,
                ns: 9,
            },
            RsParams {
                bs: 150,
                dw: 120,
                ns: 5,
            },
            RsParams {
                bs: 42,
                dw: 14,
                ns: 2,
            },
            RsParams {
                bs: 50,
                dw: 22,
                ns: 17,
            },
        ],
    },
    VersionInfo {
        idx: 19,
        data_bytes: 991,
        apat: &[6, 30, 58, 86],
        ecc: [
            RsParams {
                bs: 70,
                dw: 44,
                ns: 3,
            },
            RsParams {
                bs: 141,
                dw: 113,
                ns: 3,
            },
            RsParams {
                bs: 39,
                dw: 13,
                ns: 9,
            },
            RsParams {
                bs: 47,
                dw: 21,
                ns: 17,
            },
        ],
    },
    VersionInfo {
        idx: 20,
        data_bytes: 1085,
        apat: &[6, 34, 62, 90],
        ecc: [
            RsParams {
                bs: 67,
                dw: 41,
                ns: 3,
            },
            RsParams {
                bs: 135,
                dw: 107,
                ns: 3,
            },
            RsParams {
                bs: 43,
                dw: 15,
                ns: 15,
            },
            RsParams {
                bs: 54,
                dw: 24,
                ns: 15,
            },
        ],
    },
    VersionInfo {
        idx: 21,
        data_bytes: 1156,
        apat: &[6, 28, 50, 72, 92],
        ecc: [
            RsParams {
                bs: 68,
                dw: 42,
                ns: 17,
            },
            RsParams {
                bs: 144,
                dw: 116,
                ns: 4,
            },
            RsParams {
                bs: 46,
                dw: 16,
                ns: 19,
            },
            RsParams {
                bs: 50,
                dw: 22,
                ns: 17,
            },
        ],
    },
    VersionInfo {
        idx: 22,
        data_bytes: 1258,
        apat: &[6, 26, 50, 74, 98],
        ecc: [
            RsParams {
                bs: 74,
                dw: 46,
                ns: 17,
            },
            RsParams {
                bs: 139,
                dw: 111,
                ns: 2,
            },
            RsParams {
                bs: 37,
                dw: 13,
                ns: 34,
            },
            RsParams {
                bs: 54,
                dw: 24,
                ns: 7,
            },
        ],
    },
    VersionInfo {
        idx: 23,
        data_bytes: 1364,
        apat: &[6, 30, 54, 78, 102],
        ecc: [
            RsParams {
                bs: 75,
                dw: 47,
                ns: 4,
            },
            RsParams {
                bs: 151,
                dw: 121,
                ns: 4,
            },
            RsParams {
                bs: 45,
                dw: 15,
                ns: 16,
            },
            RsParams {
                bs: 54,
                dw: 24,
                ns: 11,
            },
        ],
    },
    VersionInfo {
        idx: 24,
        data_bytes: 1474,
        apat: &[6, 28, 54, 80, 106],
        ecc: [
            RsParams {
                bs: 73,
                dw: 45,
                ns: 6,
            },
            RsParams {
                bs: 147,
                dw: 117,
                ns: 6,
            },
            RsParams {
                bs: 46,
                dw: 16,
                ns: 30,
            },
            RsParams {
                bs: 54,
                dw: 24,
                ns: 11,
            },
        ],
    },
    VersionInfo {
        idx: 25,
        data_bytes: 1588,
        apat: &[6, 32, 58, 84, 110],
        ecc: [
            RsParams {
                bs: 75,
                dw: 47,
                ns: 8,
            },
            RsParams {
                bs: 132,
                dw: 106,
                ns: 8,
            },
            RsParams {
                bs: 45,
                dw: 15,
                ns: 22,
            },
            RsParams {
                bs: 54,
                dw: 24,
                ns: 7,
            },
        ],
    },
    VersionInfo {
        idx: 26,
        data_bytes: 1706,
        apat: &[6, 30, 58, 86, 114],
        ecc: [
            RsParams {
                bs: 74,
                dw: 46,
                ns: 19,
            },
            RsParams {
                bs: 142,
                dw: 114,
                ns: 10,
            },
            RsParams {
                bs: 46,
                dw: 16,
                ns: 33,
            },
            RsParams {
                bs: 50,
                dw: 22,
                ns: 28,
            },
        ],
    },
    VersionInfo {
        idx: 27,
        data_bytes: 1828,
        apat: &[6, 34, 62, 90, 118],
        ecc: [
            RsParams {
                bs: 73,
                dw: 45,
                ns: 22,
            },
            RsParams {
                bs: 152,
                dw: 122,
                ns: 8,
            },
            RsParams {
                bs: 45,
                dw: 15,
                ns: 12,
            },
            RsParams {
                bs: 53,
                dw: 23,
                ns: 8,
            },
        ],
    },
    VersionInfo {
        idx: 28,
        data_bytes: 1921,
        apat: &[6, 26, 50, 74, 98, 122],
        ecc: [
            RsParams {
                bs: 73,
                dw: 45,
                ns: 3,
            },
            RsParams {
                bs: 147,
                dw: 117,
                ns: 3,
            },
            RsParams {
                bs: 45,
                dw: 15,
                ns: 11,
            },
            RsParams {
                bs: 54,
                dw: 24,
                ns: 4,
            },
        ],
    },
    VersionInfo {
        idx: 29,
        data_bytes: 2051,
        apat: &[6, 30, 54, 78, 102, 126],
        ecc: [
            RsParams {
                bs: 73,
                dw: 45,
                ns: 21,
            },
            RsParams {
                bs: 146,
                dw: 116,
                ns: 7,
            },
            RsParams {
                bs: 45,
                dw: 15,
                ns: 19,
            },
            RsParams {
                bs: 53,
                dw: 23,
                ns: 1,
            },
        ],
    },
    VersionInfo {
        idx: 30,
        data_bytes: 2185,
        apat: &[6, 26, 52, 78, 104, 130],
        ecc: [
            RsParams {
                bs: 75,
                dw: 47,
                ns: 19,
            },
            RsParams {
                bs: 145,
                dw: 115,
                ns: 5,
            },
            RsParams {
                bs: 45,
                dw: 15,
                ns: 23,
            },
            RsParams {
                bs: 54,
                dw: 24,
                ns: 15,
            },
        ],
    },
    VersionInfo {
        idx: 31,
        data_bytes: 2323,
        apat: &[6, 30, 56, 82, 108, 134],
        ecc: [
            RsParams {
                bs: 74,
                dw: 46,
                ns: 2,
            },
            RsParams {
                bs: 145,
                dw: 115,
                ns: 13,
            },
            RsParams {
                bs: 45,
                dw: 15,
                ns: 23,
            },
            RsParams {
                bs: 54,
                dw: 24,
                ns: 42,
            },
        ],
    },
    VersionInfo {
        idx: 32,
        data_bytes: 2465,
        apat: &[6, 34, 60, 86, 112, 138],
        ecc: [
            RsParams {
                bs: 74,
                dw: 46,
                ns: 10,
            },
            RsParams {
                bs: 145,
                dw: 115,
                ns: 17,
            },
            RsParams {
                bs: 45,
                dw: 15,
                ns: 19,
            },
            RsParams {
                bs: 54,
                dw: 24,
                ns: 10,
            },
        ],
    },
    VersionInfo {
        idx: 33,
        data_bytes: 2611,
        apat: &[6, 30, 58, 86, 114, 142],
        ecc: [
            RsParams {
                bs: 74,
                dw: 46,
                ns: 14,
            },
            RsParams {
                bs: 145,
                dw: 115,
                ns: 17,
            },
            RsParams {
                bs: 45,
                dw: 15,
                ns: 11,
            },
            RsParams {
                bs: 54,
                dw: 24,
                ns: 29,
            },
        ],
    },
    VersionInfo {
        idx: 34,
        data_bytes: 2761,
        apat: &[6, 34, 62, 90, 118, 146],
        ecc: [
            RsParams {
                bs: 74,
                dw: 46,
                ns: 14,
            },
            RsParams {
                bs: 145,
                dw: 115,
                ns: 13,
            },
            RsParams {
                bs: 46,
                dw: 16,
                ns: 59,
            },
            RsParams {
                bs: 54,
                dw: 24,
                ns: 44,
            },
        ],
    },
    VersionInfo {
        idx: 35,
        data_bytes: 2876,
        apat: &[6, 30, 54, 78, 102, 126, 150],
        ecc: [
            RsParams {
                bs: 75,
                dw: 47,
                ns: 12,
            },
            RsParams {
                bs: 151,
                dw: 121,
                ns: 12,
            },
            RsParams {
                bs: 45,
                dw: 15,
                ns: 22,
            },
            RsParams {
                bs: 54,
                dw: 24,
                ns: 39,
            },
        ],
    },
    VersionInfo {
        idx: 36,
        data_bytes: 3034,
        apat: &[6, 24, 50, 76, 102, 128, 154],
        ecc: [
            RsParams {
                bs: 75,
                dw: 47,
                ns: 6,
            },
            RsParams {
                bs: 151,
                dw: 121,
                ns: 6,
            },
            RsParams {
                bs: 45,
                dw: 15,
                ns: 2,
            },
            RsParams {
                bs: 54,
                dw: 24,
                ns: 46,
            },
        ],
    },
    VersionInfo {
        idx: 37,
        data_bytes: 3196,
        apat: &[6, 28, 54, 80, 106, 132, 158],
        ecc: [
            RsParams {
                bs: 74,
                dw: 46,
                ns: 29,
            },
            RsParams {
                bs: 152,
                dw: 122,
                ns: 17,
            },
            RsParams {
                bs: 45,
                dw: 15,
                ns: 24,
            },
            RsParams {
                bs: 54,
                dw: 24,
                ns: 49,
            },
        ],
    },
    VersionInfo {
        idx: 38,
        data_bytes: 3362,
        apat: &[6, 32, 58, 84, 110, 136, 162],
        ecc: [
            RsParams {
                bs: 74,
                dw: 46,
                ns: 13,
            },
            RsParams {
                bs: 152,
                dw: 122,
                ns: 4,
            },
            RsParams {
                bs: 45,
                dw: 15,
                ns: 42,
            },
            RsParams {
                bs: 54,
                dw: 24,
                ns: 48,
            },
        ],
    },
    VersionInfo {
        idx: 39,
        data_bytes: 3532,
        apat: &[6, 26, 54, 82, 110, 138, 166],
        ecc: [
            RsParams {
                bs: 75,
                dw: 47,
                ns: 40,
            },
            RsParams {
                bs: 147,
                dw: 117,
                ns: 20,
            },
            RsParams {
                bs: 45,
                dw: 15,
                ns: 10,
            },
            RsParams {
                bs: 54,
                dw: 24,
                ns: 43,
            },
        ],
    },
    VersionInfo {
        idx: 40,
        data_bytes: 3706,
        apat: &[6, 30, 58, 86, 114, 142, 170],
        ecc: [
            RsParams {
                bs: 75,
                dw: 47,
                ns: 18,
            },
            RsParams {
                bs: 148,
                dw: 118,
                ns: 19,
            },
            RsParams {
                bs: 45,
                dw: 15,
                ns: 20,
            },
            RsParams {
                bs: 54,
                dw: 24,
                ns: 34,
            },
        ],
    },
];
