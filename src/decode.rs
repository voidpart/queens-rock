use super::{Code, Data, EccLevel, Payload};
use crate::bit_vec::{BitIterator, LittleEndianBitVec};
use crate::gf256::GF256;
use crate::version_db::{RsParams, MAX_VERSION};

// See ISO 18004:2006, Annex C, Table C.1
const FORMAT_DECODE_INFO: [u16; 32] = [
    (0x5412),
    (0x5125),
    (0x5E7C),
    (0x5B4B),
    (0x45F9),
    (0x40CE),
    (0x4F97),
    (0x4AA0),
    (0x77C4),
    (0x72F3),
    (0x7DAA),
    (0x789D),
    (0x662F),
    (0x6318),
    (0x6C41),
    (0x6976),
    (0x1689),
    (0x13BE),
    (0x1CE7),
    (0x19D0),
    (0x0762),
    (0x0255),
    (0x0D0C),
    (0x083B),
    (0x355F),
    (0x3068),
    (0x3F31),
    (0x3A06),
    (0x24B4),
    (0x2183),
    (0x2EDA),
    (0x2BED),
];

fn find_bit_diff(op1: u16, op2: u16, max_diffs: usize) -> usize {
    let mut diff = 0;

    for j in 0..15 {
        let mask = 1 << j;
        if op1 & mask != op2 & mask {
            diff += 1;
        }

        if diff >= max_diffs {
            break;
        }
    }

    diff
}

fn find_format(format1: u16, format2: u16) -> Result<u16, ()> {
    for target in FORMAT_DECODE_INFO.iter() {
        if format1 == *target || format2 == *target {
            return Ok(*target);
        } else {
            let diff1 = find_bit_diff(format1, *target, 4);
            if diff1 <= 3 {
                return Ok(*target);
            }

            if format1 != format2 {
                let diff2 = find_bit_diff(format2, *target, 4);
                if diff2 <= 3 {
                    return Ok(*target);
                }
            }
        }
    }

    // not correct format found
    Err(())
}

fn decode_format(format1: u16, format2: u16) -> Result<(EccLevel, usize), ()> {
    // Some QR coders don't mask format info,
    // so we try both variants
    let format = find_format(format1, format2)
        .map(|i| i ^ 0x5412)
        .or_else(|_| find_format(format1, format2))?;

    let fdata = format >> 10;
    let ecc_level = EccLevel::from_int((fdata >> 3) as isize).ok_or(())?;
    let mask = fdata & 7;

    Ok((ecc_level, mask as usize))
}

fn read_format(code: &Code) -> Result<(EccLevel, usize), ()> {
    // format in qr codes present in two places
    // we read both data and try to find best variant
    let format1 = {
        let mut format: u16 = 0;

        for i in 0..7 {
            format = (format << 1) | code.grid_bit(8, code.size - 1 - i).ok_or(())? as u16
        }
        for i in 0..8 {
            format = (format << 1) | code.grid_bit(code.size - 8 + i, 8).ok_or(())? as u16
        }

        format
    };
    let format2 = {
        const XS: [u8; 15] = [8, 8, 8, 8, 8, 8, 8, 8, 7, 5, 4, 3, 2, 1, 0];
        const YS: [u8; 15] = [0, 1, 2, 3, 4, 5, 7, 8, 8, 8, 8, 8, 8, 8, 8];

        let mut format: u16 = 0;
        for i in (0..15).rev() {
            format = (format << 1) | code.grid_bit(XS[i] as usize, YS[i] as usize).ok_or(())? as u16
        }

        format
    };

    decode_format(format1, format2)
}

fn read_data(code: &Code, data: &Data) -> Result<LittleEndianBitVec, ()> {
    let mut buf = LittleEndianBitVec::with_capacity(code.size * code.size);

    let info = data.version_info().unwrap();

    let mut y = (code.size - 1) as isize;
    let mut x = (code.size - 1) as isize;
    let mut dir: isize = -1;

    while x > 0 {
        if x == 6 {
            x -= 1;
        }

        for i in 0..2 {
            let x = (x - i) as usize;

            if !info.reserved_cell(y as usize, x) {
                let mut v = code.grid_bit(x, y as usize).expect("bit must exists");
                if data.mask_bit(x as usize, y as usize) {
                    v ^= true;
                }
                buf.push(v);
            }
        }

        if y == 0 && dir == -1 {
            dir = 1;
            x -= 2;
        } else if y == (code.size - 1) as isize && dir == 1 {
            dir = -1;
            x -= 2;
        } else {
            y += dir;
        }
    }

    // error correction
    let corrected = codestream_ecc(buf.as_ref(), data)?;
    buf.as_mut().copy_from_slice(&corrected);

    Ok(buf)
}

pub fn decode(code: &Code) -> Result<Data, ()> {
    if (code.size - 17) % 4 != 0 {
        return Err(());
    }
    let version = (code.size - 17) / 4;
    if version < 1 || version > MAX_VERSION {
        return Err(());
    }

    let (ecc_level, mask) = read_format(code)?;

    let mut data = Data::new(version as u8, ecc_level, mask);
    let buf = read_data(code, &data)?;

    decode_payload(&mut data, buf)?;

    Ok(data)
}

fn decode_payload(data: &mut Data, buf: LittleEndianBitVec) -> Result<(), ()> {
    /* QR-code data types. */
    const DATA_TYPE_TERMINATOR: usize = 0b0000;
    const DATA_TYPE_NUMERIC: usize = 0b0001;
    const DATA_TYPE_ALPHA: usize = 0b0010;
    const DATA_TYPE_BYTE: usize = 0b0100;
    #[allow(dead_code)]
    const DATA_TYPE_KANJI: usize = 0b1000;
    const DATA_TYPE_ECI: usize = 0b0111;

    let mut iter = buf.iter();

    while iter.size_hint().0 >= 4 {
        let type_ = iter.take_bits(4);

        match type_ {
            DATA_TYPE_TERMINATOR => break,

            DATA_TYPE_NUMERIC => {
                let payload = decode_numeric(data, &mut iter)?;
                data.payloads.push(payload);
            }

            DATA_TYPE_ALPHA => {
                let payload = decode_alpha(data, &mut iter)?;
                data.payloads.push(payload);
            }

            DATA_TYPE_BYTE => {
                let payload = decode_byte(data, &mut iter)?;
                data.payloads.push(payload);
            }

            DATA_TYPE_ECI => {
                let eci = decode_eci(&mut iter)?;
                data.eci = Some(eci);
            }

            //            DATA_TYPE_KANJI => {
            //                data.payloads.push(decode_kanji(data, &mut iter)?);
            //            }
            _ => break,
        }
    }

    Ok(())
}

fn take_numeric_tuple<R, I>(iter: &mut I) -> Result<R, ()>
where
    R: Default + AsMut<[char]>,
    I: Iterator<Item = bool>,
{
    let mut s = R::default();

    let bits = match s.as_mut().len() {
        1 => 4,
        2 => 7,
        3 => 10,

        _ => unreachable!(),
    };
    let mut tuple = iter.take_bits(bits);

    for x in s.as_mut().iter_mut().rev() {
        *x = ((tuple % 10) as u8 + b'0').into();

        tuple /= 10;
    }

    Ok(s)
}

fn decode_numeric<I: Iterator<Item = bool>>(data: &Data, iter: &mut I) -> Result<Payload, ()> {
    let bits = if data.version < 10 {
        10
    } else if data.version < 27 {
        12
    } else {
        14
    };

    let mut count = iter.take_bits(bits);
    let exact_bit_count = {
        let tmp = 10 * count;

        tmp / 3 + (tmp % 3 > 0) as usize
    };
    if iter.size_hint().0 < exact_bit_count {
        return Err(());
    }

    let mut str = String::with_capacity(count);
    while count >= 3 {
        let i: [char; 3] = take_numeric_tuple(iter)?;

        str.push(i[0]);
        str.push(i[1]);
        str.push(i[2]);

        count -= 3;
    }

    if count >= 2 {
        let i: [char; 2] = take_numeric_tuple(iter)?;

        str.push(i[0]);
        str.push(i[1]);

        count -= 2;
    }

    if count >= 1 {
        let i: [char; 1] = take_numeric_tuple(iter)?;

        str.push(i[0]);
    }

    Ok(Payload::Numeric(str))
}

fn decode_alpha_char(b: u8) -> char {
    const CHARMAP: &str = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ $%*+-./:";

    CHARMAP.chars().nth((b % 45) as usize).unwrap()
}

fn take_alpha_tuple<I: Iterator<Item = bool>>(iter: &mut I) -> [char; 2] {
    let tuple = iter.take_bits(11);

    [
        decode_alpha_char((tuple / 45) as u8),
        decode_alpha_char((tuple % 45) as u8),
    ]
}

fn decode_alpha<I: Iterator<Item = bool>>(data: &Data, iter: &mut I) -> Result<Payload, ()> {
    let bits = if data.version < 10 {
        9
    } else if data.version < 27 {
        11
    } else {
        13
    };

    let mut count = iter.take_bits(bits);
    let exact_bit_count = {
        let tmp = 11 * count;
        tmp / 2 + (tmp % 2)
    };
    if iter.size_hint().0 < exact_bit_count {
        return Err(());
    }

    let mut str = String::with_capacity(count);

    while count >= 2 {
        let tuple = take_alpha_tuple(iter);

        str.push(tuple[0]);
        str.push(tuple[1]);

        count -= 2;
    }

    if count > 0 {
        str.push(decode_alpha_char(iter.take_bits(6) as u8));
    }

    Ok(Payload::Alpha(str))
}

fn decode_eci<I: Iterator<Item = bool>>(iter: &mut I) -> Result<usize, ()> {
    if iter.size_hint().0 < 8 {
        return Err(());
    }

    let mut eci = iter.take_bits(8) as usize;

    if (eci & 0xc0) == 0x80 {
        if iter.size_hint().0 < 8 {
            return Err(());
        }

        eci = (eci << 8) | iter.take_bits(8) as usize;
    } else if (eci & 0xe0) == 0xc0 {
        if iter.size_hint().0 < 16 {
            return Err(());
        }

        eci = (eci << 16) | iter.take_bits(16) as usize;
    }

    Ok(eci)
}

fn decode_byte<I: Iterator<Item = bool>>(data: &Data, iter: &mut I) -> Result<Payload, ()> {
    let bits = if data.version < 10 { 8 } else { 16 };

    let count = iter.take_bits(bits);

    if iter.size_hint().0 < count * 8 {
        return Err(());
    }

    let mut buf = Vec::with_capacity(count);
    for _ in 0..count {
        buf.push(iter.take_bits(8) as u8);
    }

    Ok(Payload::Byte(buf))
}

fn codestream_ecc(src: &[u8], data: &Data) -> Result<Vec<u8>, ()> {
    let mut res = Vec::new();
    res.resize(src.len(), 0);

    let ver = data.version_info().ok_or(())?;
    let sb_ecc = data.ecc_rs_params().ok_or(())?;
    let mut lb_ecc: RsParams = *sb_ecc;

    lb_ecc.dw += 1;
    lb_ecc.bs += 1;

    let lb_count =
        (ver.data_bytes - sb_ecc.bs as usize * sb_ecc.ns as usize) / (sb_ecc.bs as usize + 1);
    let bc = lb_count + sb_ecc.ns as usize;
    let mut dst_offset = 0;
    let ecc_offset = sb_ecc.dw as usize * bc + lb_count;
    for i in 0..bc {
        let dst = &mut res[dst_offset..];

        let ecc = if i < sb_ecc.ns as usize {
            sb_ecc
        } else {
            &lb_ecc
        };

        let num_ec = ecc.bs - ecc.dw;

        for j in 0..ecc.dw as usize {
            dst[j] = src[j * bc + i];
        }

        for j in 0..num_ec as usize {
            dst[ecc.dw as usize + j] = src[ecc_offset + j * bc + i];
        }

        GF256.correct(&mut dst[..ecc.bs as usize], (ecc.bs - ecc.dw) as usize)?;

        dst_offset += ecc.dw as usize;
    }

    Ok(res)
}
