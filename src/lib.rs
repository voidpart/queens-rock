#![cfg_attr(feature = "unstable", feature(test))]
#![deny(unsafe_code)]

#[cfg(all(feature = "unstable", test))]
extern crate test;

use bit_vec::BigEndianBitVec;
use version_db::{RsParams, VersionInfo, VERSION_DB};

pub use identify::{ScanResults, Scanner};
pub use linear_algebra::Point;

mod bit_vec;
mod decode;
mod gf256;
mod identify;
mod linear_algebra;
mod pixels;
mod version_db;

#[cfg(test)]
mod test_data;
#[cfg(test)]
mod tests;

#[derive(Debug, Copy, Clone)]
pub enum EccLevel {
    M = 0,
    L,
    H,
    Q,
}

impl EccLevel {
    fn into_int(self) -> usize {
        use EccLevel::*;

        match self {
            M => 0,
            L => 1,
            H => 2,
            Q => 3,
        }
    }

    fn from_int(val: isize) -> Option<EccLevel> {
        match val {
            0 => Some(EccLevel::M),
            1 => Some(EccLevel::L),
            2 => Some(EccLevel::H),
            3 => Some(EccLevel::Q),

            _ => None,
        }
    }
}

#[derive(Debug)]
pub enum Payload {
    Numeric(String),
    Alpha(String),
    Byte(Vec<u8>),
}

/// QR payload
#[derive(Debug)]
pub struct Data {
    // Various parameters of the QR-code. These can mostly be
    // ignored if you only care about the data.
    pub version: u8,
    pub ecc_level: EccLevel,
    pub mask: usize,
    pub eci: Option<usize>,

    /// Array of payload datas extracted from QR
    pub payloads: Vec<Payload>,
}

impl Data {
    /// Try to convert all payloads to `String` and concatenate then
    pub fn try_string(&self) -> Result<String, Error> {
        let mut result = String::new();

        for i in self.payloads.iter() {
            match i {
                Payload::Alpha(ref alpha) => result += alpha,
                Payload::Numeric(ref numeric) => result += numeric,
                Payload::Byte(ref bytes) => {
                    result += String::from_utf8_lossy(bytes).as_ref();
                }
            }
        }

        Ok(result)
    }

    fn new(version: u8, ecc_level: EccLevel, mask: usize) -> Data {
        Data {
            version,
            ecc_level,
            mask,
            payloads: Vec::new(),
            eci: None,
        }
    }

    fn version_info(&self) -> Option<&'static VersionInfo> {
        VERSION_DB.get(self.version as usize)
    }

    fn ecc_rs_params(&self) -> Option<&'static RsParams> {
        self.version_info()
            .and_then(|i| i.ecc.get(self.ecc_level.into_int()))
    }

    fn mask_bit(&self, x: usize, y: usize) -> bool {
        match self.mask {
            0 => (y + x) % 2 == 0,
            1 => (y % 2) == 0,
            2 => (x % 3) == 0,
            3 => ((y + x) % 3) == 0,
            4 => (((y / 2) + (x / 3)) % 2) == 0,
            5 => ((y * x) % 2 + (y * x) % 3) == 0,
            6 => (((y * x) % 2 + (y * x) % 3) % 2) == 0,
            7 => (((y * x) % 3 + (y + x) % 2) % 2) == 0,

            _ => unreachable!(),
        }
    }
}

pub struct Code {
    pub corners: [Point; 4],
    pub size: usize,
    cell_bitmap: BigEndianBitVec,
}

pub enum Error {
    Unknown,
}

impl Code {
    /// Try to decode QR payload
    pub fn decode(&self) -> Result<Data, Error> {
        decode::decode(self).map_err(|_| Error::Unknown)
    }

    fn grid_bit(&self, x: usize, y: usize) -> Option<bool> {
        self.cell_bitmap.get(y * self.size + x)
    }
}
