use std::cell::RefCell;
use std::rc::Rc;

use crate::identify::{RcRegion, Region};

#[derive(Debug, Clone)]
pub enum Pixel {
    Origin(u8),
    White,
    Black,
    Region(Rc<RefCell<Region>>),
}

impl Pixel {
    pub fn is_white(&self) -> bool {
        match self {
            Pixel::White => true,
            _ => false,
        }
    }

    pub fn is_black(&self) -> bool {
        match self {
            Pixel::Black => true,
            _ => false,
        }
    }

    pub fn get_origin(&self) -> Option<u8> {
        match self {
            Pixel::Origin(val) => Some(*val),

            _ => None,
        }
    }

    pub fn get_region(&self) -> Option<&RcRegion> {
        match self {
            Pixel::Region(ref reg) => Some(reg),

            _ => None,
        }
    }
}

impl PartialEq for Pixel {
    fn eq(&self, other: &Pixel) -> bool {
        match self {
            Pixel::White => other.is_white(),
            Pixel::Black => other.is_black(),
            Pixel::Origin(ref this) => other.get_origin().map(|i| *this == i).unwrap_or(false),
            Pixel::Region(ref this) => other
                .get_region()
                .map(|i| Rc::ptr_eq(this, i))
                .unwrap_or(false),
        }
    }
}

pub struct Pixels {
    buffer: Vec<Pixel>,
    w: usize,
    h: usize,
}

impl Pixels {
    pub(crate) fn from_iter<I: Iterator<Item = Pixel>>(
        iter: I,
        w: usize,
        h: usize,
    ) -> Result<Pixels, ()> {
        let buffer: Vec<Pixel> = iter.take(w * h).map(Into::into).collect();

        if buffer.len() == w * h {
            Ok(Pixels { buffer, w, h })
        } else {
            Err(())
        }
    }

    fn pixel_offset(&self, x: usize, y: usize) -> Option<usize> {
        if y < self.height() && x < self.width() {
            Some(y * self.width() + x)
        } else {
            None
        }
    }

    pub fn get_pixel(&self, x: usize, y: usize) -> Option<&Pixel> {
        self.pixel_offset(x, y)
            .and_then(move |i| self.buffer.get(i))
    }

    pub fn get_row_mut(&mut self, y: usize) -> Option<&mut [Pixel]> {
        if y >= self.h {
            None
        } else {
            let s = y * self.w;
            Some(&mut self.buffer[s..(s + self.w)])
        }
    }

    pub fn iter_rows_mut(&mut self) -> impl Iterator<Item = &mut [Pixel]> {
        let (w, h) = self.dimensions();

        self.buffer.chunks_mut(w).take(h)
    }

    pub fn width(&self) -> usize {
        self.w
    }

    pub fn height(&self) -> usize {
        self.h
    }

    pub fn dimensions(&self) -> (usize, usize) {
        (self.width(), self.height())
    }

    /// Adaptive thresholding
    pub fn threshold(&mut self) {
        use std::cmp;

        const THRESHOLD_S_MIN: usize = 1;
        const THRESHOLD_S_DEN: usize = 8;
        const THRESHOLD_T: usize = 95;

        let w = self.width();
        let (mut avg_w, mut avg_u) = (0, 0);
        let threshold_s = cmp::max(w / THRESHOLD_S_DEN, THRESHOLD_S_MIN);
        let mut row_average: Vec<usize> = vec![0; w];

        for (y, row) in self.iter_rows_mut().enumerate() {
            for x in 0..w {
                let (w, u) = if y & 1 != 0 {
                    (x, w - 1 - x)
                } else {
                    (w - 1 - x, x)
                };

                avg_w = (avg_w * (threshold_s - 1)) / threshold_s
                    + row[w].get_origin().unwrap() as usize;
                avg_u = (avg_u * (threshold_s - 1)) / threshold_s
                    + row[u].get_origin().unwrap() as usize;

                row_average[w] += avg_w;
                row_average[u] += avg_u;
            }

            for (val, avg) in row.iter_mut().zip(row_average.iter()) {
                let threshold_val = avg * THRESHOLD_T / (200 * threshold_s);

                *val = if (val.get_origin().unwrap() as usize) < threshold_val {
                    Pixel::Black
                } else {
                    Pixel::White
                };
            }

            row_average.iter_mut().for_each(|i| *i = 0);
        }
    }

    pub fn flood_fill_seed<S: Spanner>(
        &mut self,
        x: usize,
        y: usize,
        from: Pixel,
        to: Pixel,
        mut spanner: S,
    ) -> S::Result {
        self.flood_fill_seed_inner(x, y, from, to, &mut spanner, 0);

        spanner.result()
    }

    fn flood_fill_seed_inner<S: Spanner>(
        &mut self,
        x: usize,
        y: usize,
        from: Pixel,
        to: Pixel,
        func: &mut S,
        depth: isize,
    ) {
        const FLOOD_FILL_MAX_DEPTH: isize = 4096;

        let mut left = x;
        let mut right = x;
        let (w, h) = self.dimensions();

        {
            let row = self.get_row_mut(y).unwrap();

            if depth >= FLOOD_FILL_MAX_DEPTH {
                return;
            }

            while left > 0 && row[(left - 1) as usize] == from {
                left -= 1;
            }

            while right < (w - 1) && row[(right + 1) as usize] == from {
                right += 1;
            }

            /* Fill the extent */
            for i in row[(left..=right)].iter_mut() {
                *i = to.clone();
            }
        }

        func.span(y, left, right);

        {
            let range = left..=right;

            /* Seed new flood-fills */
            if y > 0 {
                for i in range.clone() {
                    let x = self.get_pixel(i, y - 1).unwrap().clone();

                    if x == from.clone() {
                        self.flood_fill_seed_inner(
                            i,
                            y - 1,
                            from.clone(),
                            to.clone(),
                            func,
                            depth + 1,
                        );
                    }
                }
            }

            if y < h - 1 {
                for i in range.clone() {
                    let x = self.get_pixel(i, y + 1).unwrap().clone();

                    if x == from {
                        self.flood_fill_seed_inner(
                            i,
                            y + 1,
                            from.clone(),
                            to.clone(),
                            func,
                            depth + 1,
                        );
                    }
                }
            }
        }
    }
}

pub trait Spanner {
    type Result;

    fn span(&mut self, x: usize, left: usize, right: usize);
    fn result(self) -> Self::Result;
}

#[cfg(all(feature = "unstable", test))]
mod bench {
    use image::GenericImageView;

    use test::Bencher;

    use super::{Pixel, Pixels};
    use crate::test_data::IMAGES;

    #[bench]
    fn bench_threshold(b: &mut Bencher) {
        let pic = &IMAGES[0];
        let img = (pic.to_luma(), pic.width(), pic.height());

        b.iter(|| {
            let mut pixels = Pixels::from_iter(
                img.0.iter().map(|i| Pixel::Origin(*i)),
                img.1 as usize,
                img.2 as usize,
            )
            .unwrap();

            pixels.threshold();
        });
    }
}
