use image;
use lazy_static::lazy_static;

pub const IMAGE_DATA: [&[u8]; 5] = [
    include_bytes!("test_data/qr.png"),
    include_bytes!("test_data/qr_multi.png"),
    include_bytes!("test_data/qr_ver10.png"),
    include_bytes!("test_data/qr_ver40.png"),
    include_bytes!("test_data/qr_damaged.jpg"),
];

lazy_static! {
    pub static ref IMAGES: Vec<image::DynamicImage> = {
        IMAGE_DATA
            .iter()
            .map(|i| image::load_from_memory(i).unwrap())
            .collect()
    };
}
