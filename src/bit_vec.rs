#![allow(dead_code)]

use std::marker::PhantomData;

pub trait Endian {
    fn bit_position(idx: usize) -> usize;

    fn write_bit(block: u8, idx: usize, value: bool) -> u8 {
        if value {
            block | (1 << Self::bit_position(idx))
        } else {
            block & !(1 << Self::bit_position(idx))
        }
    }

    fn read_bit(block: u8, idx: usize) -> bool {
        block & (1 << Self::bit_position(idx)) != 0
    }
}

#[derive(Debug)]
pub struct BigEndian;

impl Endian for BigEndian {
    fn bit_position(idx: usize) -> usize {
        idx
    }
}

#[derive(Debug)]
pub struct LittleEndian;

impl Endian for LittleEndian {
    fn bit_position(idx: usize) -> usize {
        7 - idx
    }
}

pub type BigEndianBitVec = BitVec<BigEndian>;
pub type LittleEndianBitVec = BitVec<LittleEndian>;

#[derive(Debug)]
pub struct BitVec<O: Endian> {
    buf: Vec<u8>,
    len: usize,
    order: PhantomData<O>,
}

impl<E: Endian> BitVec<E> {
    fn vec_size(bit_size: usize) -> usize {
        let mut vec_size = bit_size >> 3;

        if bit_size & 7 != 0 {
            vec_size += 1;
        }

        vec_size
    }

    pub fn with_capacity(size: usize) -> BitVec<E> {
        let vec_size = BitVec::<E>::vec_size(size);
        let buf = Vec::with_capacity(vec_size);

        BitVec {
            buf,
            len: 0,
            order: Default::default(),
        }
    }

    pub fn new() -> BitVec<E> {
        BitVec {
            buf: Vec::new(),
            len: 0,
            order: Default::default(),
        }
    }

    pub fn from_raw(raw: Vec<u8>) -> BitVec<E> {
        let len = raw.len() * 8;

        BitVec {
            len,
            buf: raw,
            order: Default::default(),
        }
    }

    pub fn len(&self) -> usize {
        self.len
    }

    pub fn get(&self, i: usize) -> Option<bool> {
        if i >= self.len {
            return None;
        }

        self.buf.get(i >> 3).map(|byte| E::read_bit(*byte, i & 7))
    }

    pub fn set(&mut self, i: usize, val: bool) {
        if i >= self.len {
            panic!("index out of bounds")
        }

        let byte_pos = i >> 3;
        if let Some(byte) = self.buf.get_mut(byte_pos) {
            *byte = E::write_bit(*byte, i & 7, val);
        }
    }

    pub fn resize(&mut self, new_size: usize) {
        self.buf.resize(BitVec::<E>::vec_size(new_size), 0);

        // Write zero to bit pos
        // This allows skip write false bit on push
        if new_size < self.len {
            let bit_pos = new_size & 7;
            if bit_pos != 0 && !self.buf.is_empty() {
                let last_idx = self.buf.len() - 1;

                if let Some(last) = self.buf.get_mut(last_idx) {
                    for i in bit_pos..8 {
                        *last = E::write_bit(*last, i, false);
                    }
                }
            }
        }

        self.len = new_size;
    }

    pub fn push(&mut self, bit: bool) {
        let byte_pos = self.len >> 3;
        if byte_pos >= self.buf.len() {
            self.buf.push(0)
        }

        // skip write false bit because we zeroing memory in `pop` and `resize`
        // so now we assume that memory are zeroed
        // this allow to optimize write for some performance issue on `pop`
        if bit {
            if let Some(byte) = self.buf.get_mut(byte_pos) {
                *byte = E::write_bit(*byte, self.len & 7, true);
            }
        }

        self.len += 1;
    }

    pub fn pop(&mut self) -> Option<bool> {
        if self.len == 0 {
            return None;
        }

        let idx = self.len - 1;
        let bit_pos = idx & 7;

        if let Some(val) = self.get(idx) {
            // Write zero to bit pos
            // This allows skip write false bit on push
            if bit_pos == 0 {
                self.buf.pop();
            } else {
                let last_idx = self.buf.len() - 1;
                if let Some(last) = self.buf.get_mut(last_idx) {
                    *last = E::write_bit(*last, bit_pos, false);
                }
            }
            self.len -= 1;

            Some(val)
        } else {
            None
        }
    }

    pub fn capacity(&self) -> usize {
        self.buf.capacity() * 8
    }

    pub fn into_raw(self) -> Vec<u8> {
        self.buf
    }

    pub fn iter(&self) -> Iter<E> {
        Iter { buf: self, i: 0 }
    }
}

impl<O: Endian> AsRef<[u8]> for BitVec<O> {
    fn as_ref(&self) -> &[u8] {
        self.buf.as_ref()
    }
}

impl<O: Endian> AsMut<[u8]> for BitVec<O> {
    fn as_mut(&mut self) -> &mut [u8] {
        self.buf.as_mut()
    }
}

pub trait BitIterator {
    fn take_bits(&mut self, count: u8) -> usize;
}

impl<I: Iterator<Item = bool>> BitIterator for I {
    fn take_bits(&mut self, count: u8) -> usize {
        use std::{cmp, mem};

        let count = cmp::min(count, mem::size_of::<usize>() as u8 * 8);

        let mut a = 0;
        for _ in 0..count {
            let x = match self.next() {
                Some(v) => v,
                None => break,
            };

            a <<= 1;

            if x {
                a |= 1
            }
        }

        a
    }
}

pub struct Iter<'a, O: Endian + 'a> {
    buf: &'a BitVec<O>,
    i: usize,
}

impl<'a, O: Endian> Iterator for Iter<'a, O> {
    type Item = bool;

    fn next(&mut self) -> Option<Self::Item> {
        if let Some(val) = self.buf.get(self.i) {
            self.i += 1;
            Some(val)
        } else {
            None
        }
    }

    fn size_hint(&self) -> (usize, Option<usize>) {
        let size = self.buf.len - self.i;

        (size, Some(size))
    }
}

#[cfg(test)]
mod tests {
    use super::{BigEndianBitVec, BitIterator, LittleEndianBitVec};

    #[test]
    fn test_capacity() {
        assert_eq!(BigEndianBitVec::with_capacity(0).capacity(), 0);
        assert_eq!(BigEndianBitVec::with_capacity(1).capacity(), 8);
        assert_eq!(BigEndianBitVec::with_capacity(1024).capacity(), 1024);
        assert_eq!(BigEndianBitVec::with_capacity(1025).capacity(), 1032);

        {
            let mut buf = BigEndianBitVec::new();
            buf.push(true);
            assert_eq!(buf.capacity(), 8);
        }

        {
            let mut buf = BigEndianBitVec::new();
            buf.resize(1024);
            assert_eq!(buf.capacity(), 1024);
        }
    }

    #[test]
    fn test_len() {
        assert_eq!(BigEndianBitVec::new().len(), 0);
        assert_eq!(BigEndianBitVec::with_capacity(8).len(), 0);

        {
            let mut buf = BigEndianBitVec::new();
            buf.push(true);
            assert_eq!(buf.len(), 1);
        }

        {
            let mut buf = BigEndianBitVec::new();
            buf.resize(1024);
            assert_eq!(buf.len(), 1024);
            buf.push(false);
            assert_eq!(buf.len(), 1025);
        }
    }

    #[test]
    fn test_push_pop() {
        assert_eq!(BigEndianBitVec::new().pop(), None);
        assert_eq!(BigEndianBitVec::with_capacity(1).pop(), None);

        {
            let mut buf = BigEndianBitVec::new();
            buf.push(true);
            buf.push(true);
            buf.push(false);
            buf.push(false);
            buf.push(true);

            assert_eq!(buf.pop(), Some(true));
            assert_eq!(buf.pop(), Some(false));
            assert_eq!(buf.pop(), Some(false));
            assert_eq!(buf.pop(), Some(true));
            assert_eq!(buf.pop(), Some(true));
            assert_eq!(buf.pop(), None);
        }

        {
            let mut buf = BigEndianBitVec::new();

            buf.push(true);
            assert_eq!(buf.pop(), Some(true));

            buf.push(false);
            assert_eq!(buf.get(0), Some(false));
        }
    }

    #[test]
    fn test_resize() {
        {
            let mut buf = BigEndianBitVec::with_capacity(1024);
            buf.resize(0);
            assert_eq!(buf.capacity(), 1024);
        }

        {
            let mut buf = BigEndianBitVec::new();
            for _ in 0..8 {
                buf.push(true);
            }

            buf.resize(0);

            for _ in 0..4 {
                buf.push(false);
                buf.push(true);
            }

            assert_eq!(buf.get(0), Some(false));
            assert_eq!(buf.get(1), Some(true));
            assert_eq!(buf.get(2), Some(false));
            assert_eq!(buf.get(3), Some(true));
            assert_eq!(buf.get(4), Some(false));
            assert_eq!(buf.get(5), Some(true));
            assert_eq!(buf.get(6), Some(false));
            assert_eq!(buf.get(7), Some(true));
            assert_eq!(buf.get(8), None);
        }
    }

    #[test]
    fn test_iter() {
        assert_eq!(BigEndianBitVec::new().iter().count(), 0);
        assert_eq!(BigEndianBitVec::with_capacity(1024).iter().count(), 0);
        assert_eq!(BigEndianBitVec::new().iter().size_hint(), (0, Some(0)));

        {
            let mut buf = BigEndianBitVec::new();
            buf.push(true);
            buf.push(true);
            buf.push(false);
            buf.push(false);
            buf.push(true);

            let vec: Vec<bool> = buf.iter().collect();
            assert_eq!(vec, [true, true, false, false, true])
        }
    }

    #[test]
    fn test_bit_iterator() {
        let mut buf = BigEndianBitVec::new();
        assert_eq!(buf.iter().take_bits(8), 0b0);

        buf.push(true);
        buf.push(true);
        buf.push(false);
        buf.push(false);
        buf.push(true);

        assert_eq!(buf.iter().take_bits(5), 0b11001);
        assert_eq!(buf.iter().take_bits(32), 0b11001);

        buf.push(true);
        assert_eq!(buf.iter().take_bits(32), 0b110011);
    }

    #[test]
    fn test_bit_order() {
        {
            let mut buf = BigEndianBitVec::new();
            buf.push(true);
            buf.push(true);
            buf.push(true);

            assert_eq!(buf.as_ref()[0], 0b00000111);
        }

        {
            let mut buf = LittleEndianBitVec::new();
            buf.push(true);
            buf.push(true);
            buf.push(true);

            assert_eq!(buf.as_ref()[0], 0b11100000);
        }
    }
}
