use image;
use image::GenericImageView;

use crate::identify::ScanResults;
use crate::test_data::IMAGES;

#[test]
fn test() {
    let image = &IMAGES[0];

    let result = scan_image(&image).extract(0).unwrap();
    let data = result.decode().ok().unwrap();
    assert_eq!(data.payloads.len(), 1);
    assert_eq!("Hello world!", data.try_string().ok().unwrap());
}

#[test]
fn test_scan() {
    let image = &IMAGES[0];

    assert_eq!(scan_image(&image).len(), 1);
    assert_eq!(scan_image(&image.rotate90()).len(), 1);
    assert_eq!(scan_image(&image.rotate180()).len(), 1);
    assert_eq!(scan_image(&image.rotate270()).len(), 1);
}

#[test]
fn test_scan_multi() {
    let image = &IMAGES[1];

    assert_eq!(scan_image(&image).len(), 2);
    assert_eq!(scan_image(&image.rotate270()).len(), 2);

    // for some reason algorithm can't find second qr code
    assert_eq!(scan_image(&image.rotate90()).len(), 1);
    assert_eq!(scan_image(&image.rotate180()).len(), 1);
}

#[test]
fn test_scan_ver10() {
    let image = &IMAGES[2];

    let result = scan_image(&image);
    assert_eq!(result.len(), 1);
    let data = result.extract(0).unwrap().decode().ok().unwrap();

    // Payload in several peaces of different modes
    assert_eq!(5, data.payloads.len());
    assert_eq!("VERSION 10 QR CODE, UP TO 174 CHAR AT H LEVEL, WITH 57X57 MODULES AND PLENTY OF ERROR CORRECTION TO GO AROUND.  NOTE THAT THERE ARE ADDITIONAL TRACKING BOXES",
               data.try_string().ok().unwrap());
}

#[test]
fn test_decode_ver40() {
    let image = &IMAGES[3];

    let result = scan_image(&image);
    assert_eq!(result.len(), 1);
    let data = result.extract(0).unwrap().decode().ok().unwrap();
    assert_eq!(data.payloads.len(), 1);
}

#[test]
fn test_decode_damaged() {
    let image = &IMAGES[4];

    let result = scan_image(&image);
    assert_eq!(result.len(), 1);
    let data = result.extract(0).unwrap().decode().ok().unwrap();
    assert_eq!(data.payloads.len(), 1);
    assert_eq!(data.try_string().ok().unwrap(), "http://en.m.wikipedia.org");
}

fn scan_image(image: &image::DynamicImage) -> ScanResults {
    let (w, h) = (image.width(), image.height());

    let scanner = super::Scanner::new(image.to_luma().into_vec().as_ref(), w as usize, h as usize);

    scanner.scan()
}

#[cfg(all(feature = "unstable", test))]
mod bench {
    use image::GenericImageView;

    use test::Bencher;

    use super::scan_image;
    use crate::test_data::IMAGES;

    #[bench]
    fn e2e(b: &mut Bencher) {
        let image = &IMAGES[3];

        b.iter(|| {
            let result = scan_image(&image).extract(0).unwrap();
            let _data = result.decode().ok().unwrap();
        });
    }

    #[bench]
    fn extract(b: &mut Bencher) {
        let image = &IMAGES[3];
        let result = scan_image(&image);

        b.iter(|| result.extract(0));
    }

    #[bench]
    fn identify(b: &mut Bencher) {
        let image = &IMAGES[3];
        let luma = image.to_luma().into_vec();
        let (w, h) = (image.width(), image.height());

        b.iter(|| {
            let result = crate::Scanner::new(luma.as_ref(), w as usize, h as usize).scan();
            assert_eq!(result.len(), 1)
        });
    }

    #[bench]
    fn decode(b: &mut Bencher) {
        let image = &IMAGES[3];
        let result = scan_image(&image).extract(0).unwrap();

        b.iter(|| {
            let _data = result.decode().ok().unwrap();
        });
    }
}
